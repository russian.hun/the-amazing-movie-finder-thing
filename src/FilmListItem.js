import * as React from 'react';
import { useState } from "react";
// UI library stuff
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';

/*
 * The other, smaller (could say minor) magic happens here, we need to find more details, somehow
 */
export default function FilmListItem(props) { 

    /*
     * List items get their own state
     * @detailsData will hold the data of the details. :) (Bad name, it holds the extract)
     * @wikiLink will hold the generated link, as seen in the instructions
     */
    const [detailsData, setDetailsData] = useState(undefined);
    const [wikiLink, setWikiLink] = useState(undefined);

    // Get the extract from the Wikipedia page. What a horrid API, that...
    async function wikiSearch(queryString) {
        // Look at that string. It makes my eyes bleed. But who am I to question the docs?
        const endpoint = 'https://en.wikipedia.org/w/api.php?format=json&action=query&&indexpageids&prop=extracts&&exintro&explaintext&redirects=1&origin=*&titles=' + encodeURIComponent(queryString);
        const response = await fetch(endpoint);
        
        if (!response.ok) {
            setDetailsData("Failed to fetch film details.")
        }

        /*
        * The received JSON will have funny numbers as pageID, which we don't know. Fortunately pageIds can also be indexed, 
        * so we do that, to be able to extract the extract details later.
        */ 
        const result = await response.json();
        const pageId = result.query.pageids[0];
           
        var extract;
        var url;
        var linkComponent

        // In some cases the extract simply does not exist. Why is still a mystery, but probably not every bad film has a full Wikipedia page?
        if (result.query.pages[pageId].extract != undefined){

            // 320 characters is arbitary, but seems to be enough
            extract = result.query.pages[pageId].extract.substring(0, 320) + "...";
            url = "https://en.wikipedia.org/?curid=" + pageId;
            linkComponent = <Link 
                                href={url} 
                                target="_blank" 
                                rel="noopener"> 
                                Read more
                            </Link>   
        } else {
            // Extraction failed, since there was nothing to extract, for the was no extract to extract.
            extract = "The extract could not be extracted. (The Wiki page might be broken, or non-existent. Or something.)"
            linkComponent = null
        }
        
        // Finally set the right states
        setDetailsData(extract);
        setWikiLink(linkComponent);
        
    }

    // Just sanitise the categories a bit
    const categoryString = props.categories.join(', ');

    return (
        <ListItem alignItems="flex-start" >
            <ListItemText
                primary= {
                    <Typography
                        component="h1"
                        variant="h5"
                        color="text.primary"
                        >
                            <Link 
                                style={{cursor:'pointer'}}                                   // Yeah, a <Link> without href won't do that by itself...
                                color="inherit"
                                underline="hover" 
                                onClick={ () => {
                                   
                                    if (detailsData === undefined) {                         // If we're not displaying anything...
                                        setDetailsData("Fetching details from Wikipedia...") // This is our "Spinner".So much simpler this way...
                                        wikiSearch(props.title);                             // After setting up expectations with the above text, we execute the search
                                    } else {                                                 // Close the details if user clicks title again
                                        setDetailsData(undefined); 
                                        setWikiLink(undefined);
                                    }
                                }}>
                                    {props.title}
                            </Link>
                    </Typography>    
                }                                
                secondary={
                    <React.Fragment>
                        <Typography
                            sx={{ display: 'block' }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                            >
                            {categoryString}
                        </Typography>

                        <Typography
                            sx={{ display: 'block' }}
                            component="span"
                            variant="body1"
                            color="text.primary"
                            >
                            {detailsData}   
                            {wikiLink}
                        </Typography>
                            
                        Rating: {props.rating}    
                    </React.Fragment>              
                }
            />
        </ListItem>
    );
}