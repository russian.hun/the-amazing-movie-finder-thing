import * as React from 'react';
import { useState } from "react";
// UI library stuff
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import InputAdornment from '@mui/material/InputAdornment';
import Typography from '@mui/material/Typography';
// Custom components
import Spinner from './Spinner'
import ErrorMsg from'./ErrorMsg'
import SearchResult from './SearchResult'
import { useLazyQuery, gql } from "@apollo/client";


// The majestic query. Only what we need
const GET_FILMS = gql`
      query GetFilmData($queryString: String!) {
        searchMovies(query: $queryString) {
          name
          score
          genres {
            name
          }
        }
      }
  `;

// Probably could have kept things more modular, but this way seems more straightforward. 
export default function App() {

  /*
   * State consts to hold states. 
   * @queryString is whatever the user types in the textbox, updated automagically, 
   * @getFilmData will hold the gql query result
   * Using "useLazyQuery" so it only executes on user click/Enter press
   */
  const [queryString, setQueryString] = useState("");
  const [getFilmData, {data, loading, error} ] = useLazyQuery(GET_FILMS);

  // Component to display results conditionally
  let searchResult = null;

  if (loading) {
    searchResult = <Spinner />;                                  // Spinner while loading, as requested
  } else if (error) {
    searchResult = <ErrorMsg message={error.message} />;         // Error gets displayed to user
  } else {
    if (data != undefined){
      searchResult = <SearchResult data={data.searchMovies} />;  // Search results get displayed
    }
  }

  return (
    // Kept the UI minimal, also I'm not a designer :)
    <Container maxWidth="md">
          <Box className="centeredFlexBox topMargin-3">
            <Typography variant="h4" gutterBottom component="h1">
                The Amazing Movie Finder Thing
            </Typography>
          </Box>
          <Box sx={{ my: 4 }}>
            <TextField 
              fullWidth 
              id="outlined-basic" 
              label="Find that film" 
              variant="outlined" 
              onChange={(e) => setQueryString(e.target.value)}                                    // Querystring gets updated as the user types
              /*
               * Personal note: 
               * I don't like shorthands or arrow functions a lot, 
               * but it seems to be how the cool kidz do it nowadays. 
               * Am I too old?
               */
              onKeyPress={(e) => e.key === "Enter" && getFilmData({ variables: { queryString }})} // Enter press will execute the query.
              type="string"
              InputProps={{
                  endAdornment: (
                      <InputAdornment position="end">
                          <Button 
                          variant="contained"
                          onClick={() =>{getFilmData({ variables: { queryString }})}}             // Button press will also execute the query
                          >
                            Search
                          </Button>
                      </InputAdornment>
                  ),
              }}
            />
            {searchResult}
          </Box>
        </Container>
    );
}
