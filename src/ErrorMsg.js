import * as React from 'react';
// UI library stuff
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
// Some custom styling
import './componentstyles.css';

// A very simple error message. No comment. :)
export default function ErrorMsg(props) {
  
    return (
        <Paper className="resultsPaper topMargin-3 sideMargin-3">
            <Box className="centeredFlexBox container-10">
                <Typography 
                    variant="h6" 
                    gutterBottom component="div">
                        The query returned an error:
                        <br />
                        <pre>{props.message}</pre>
                </Typography>
                
            </Box>   
        </Paper>     
    )

  }
