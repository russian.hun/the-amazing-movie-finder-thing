import * as React from 'react';
// UI library stuff
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
// Custom component
import FilmListItem from './FilmListItem'

/*
 * This is where the magic happens.
 * props holds the GQL query result  handed down from the parent
 */
export default function SearchResults(props) {   
    // Map the query result(s) into a nice wee array
    const resultsArray = props.data.map(item => {
        /*
         * And an array within the array holds the genres. 
         * The instructions weren't clear about "category", so I went with genres
         */ 
        const genreArray = item.genres.map(genre => genre.name);
        return {
            title: item.name,      
            rating: item.score,
            genres: genreArray 
        }
    });

    //Just a divider to be displayed between list items
    const dividerLine = <Divider variant="middle" component="li" />;

    //The variable will hold the results as a list to be displayed or the message questioning existence...
    var resultsList;

    // Only if we have found something
    if (resultsArray.length > 0){
        /*
         * We map our array into another aray, because we love mapping arrays. 
         * Also, it gets displayed as a List, 
         * which makes sense, since that's what the instructions say.
         * Divider will not be displeyed after the last element
         */
        resultsList =<List sx={{ width: 'auto', bgcolor: 'background.paper' }}>   
            {resultsArray.map((item, index) =>
                <React.Fragment key={index.toString()}>
                    <FilmListItem 
                        title={item.title} 
                        rating={item.rating} 
                        categories={item.genres} />
                    {(index) < resultsArray.length-1 && dividerLine} 
                </React.Fragment>
            )}
        </List>;
    } else {
        //In case we've found nothing, we go deep into philosophy...
        resultsList = <Box className="centeredFlexBox container-10">     
                         <Typography 
                            variant="h6" 
                            gutterBottom 
                            component="div">
                                The search yielded no results. Are you sure that film even exists?
                                <br />(Does anything, really? Or is 'nothing' what we've been looking for?)
                          </Typography>
                      </Box>        
    }
 
    return (
        //Finally desplay our final results, in a final manner
        <Container >
            <Paper className="resultsPaper topMargin-3 sideMargin-3">
                <Box className="centeredBox paddedBox">
                    {resultsList}
                </Box>
            </Paper>     
       
        </Container>
    )   
  }