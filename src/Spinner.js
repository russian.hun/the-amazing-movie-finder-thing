import * as React from 'react';
//UI library stuff
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import CircularProgress from '@mui/material/CircularProgress';
import Container from '@mui/material/Container';
//Some custom styling
import './componentstyles.css';

//A very simple spinner. No comment. :)
export default function Spinner() {
     
        return (
            <Container sx={{ width: '100%' }}>
                <Box className ="centeredBox topMargin-5 container-5">
                    <Typography 
                        variant="h6" 
                        gutterBottom 
                        component="div" 
                        sx={{ my:5 }}>
                            Loading stuff...
                    </Typography>
                </Box>
                <Box className="centeredFlexBox container-3">    
                    <CircularProgress />
                </Box>      
            </Container>  
        )

}
  


