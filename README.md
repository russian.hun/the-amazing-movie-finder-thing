Hi,

Here's the final version of the assessment task. Please excuse my JavaScript, I had not used it for some years, and only had 2 days for a refresher. It still feels like walking over broken glass with bare feet, but I am getting somewhere.

React was also new to me (never used it), in the few days I had to prepare, this is what I could pick up.

I kind of got stuck on the Wikipedia thingy, that was the other delay, docs were a bit confusing at times, and the returned results were ugly, but it's finally done.

Code is commented where needed.

**Point by point:**

- ✓  A working web page.                 *(It works)*
- ✓  Spinner while loading data...       *(It kind of spins, but one of them is text only)*
- ✕  Search for related movies            *(Not today, sorry, ran out of time..)*
- ✓  Bonus # 1: Use Material-UI library   *(I know it was bonus, and I skipped another task, but React docs said to start from design, and add functionality later.
                                          It was so much easier to use an existing library than to start writng ugly HTML, so I did that)*
- ✓  Bonus # 2: Tests                     *(Not a chance at my current level, sorry)*


